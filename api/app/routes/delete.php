<?php
//eliminar una entidad por su clave
$app->delete('/entidades/:id', function($id) use($app) {

include 'librerias/validaciones.php';
  validaIds($id,$app);
  $numero=0;
  $existe_id=$app->db->query("select count(id) as num from entidad2 where id=$id");
      foreach ($existe_id as $idt) {
    $numero= $idt['num'];
  }
  valida_existencia_id($numero,$app);
  $borrar_entidad_v = $app->db->query("delete from valor2 where id_entidad=$id");
  $borrar_entidad_m = $app->db->query("delete from municipio2 where entidad=$id");
  $borrar_entidad = $app->db->query("delete from entidad2 where id=$id");
validaDelete($borrar_entidad,$app,$id);
});

$app->delete('/entidades/:id/municipios/:idm', function($id,$idm) use($app) {

include 'librerias/validaciones.php';
  validaIds($id,$app);
  validaIds($idm,$app);
  $numero=0;
  $existe_id=$app->db->query("select count(id)as num from municipio2 where entidad=$id and codigo='".$idm."'");
        foreach ($existe_id as $idt) {
    $numero= $idt['num'];
  }
  valida_existencia_id($numero,$app);
    foreach ($existe_id as $idt) {
    $id_municipio= $idt['id'];
  }
    valida_existencia_id($numero,$app);
  $borrar_municipio_v = $app->db->query("delete from valor2 where id_mun=(select id from municipio2 where codigo='".$idm."' and entidad=$id)");
  $borrar_municipio= $app->db->query("delete from municipio2 where codigo='".$idm."' and entidad=$id");
validaDeleteMun($borrar_municipio,$app,$idm);
});

$app->delete('/indicadores/:id', function($id) use($app) {

include 'librerias/validaciones.php';
$numero=0;
  $existe_id=$app->db->query("select count(id_indicador) as num from indicador where id_indicador='".$id."'");
          foreach ($existe_id as $idt) {
    $numero= $idt['num'];
  }
  valida_existencia_id($numero,$app);
  $borrar_indicador_v = $app->db->query("delete from valor2 where id_ind='".$id."'");
  $borrar_indicador= $app->db->query("delete from indicador where id_indicador='".$id."'");
validaDeleteIndicador($borrar_indicador,$app,$id);
});


$app->delete('/valores/:id', function($id) use($app) {

include 'librerias/validaciones.php';
  validaIds($id,$app);
  $existe_id=$app->db->query("select id from valor2 where id=$id");
  valida_existencia_id($existe_id,$app);
  $borrar_valor_anio = $app->db->query("delete from anio where valor=$id");
  $borrar_valor= $app->db->query("delete from valor2 where id=$id");
  validaDeleteValor($borrar_indicador,$app,$id);
});

$app->delete('/niveles1/:id', function($id) use($app) {

include 'librerias/validaciones.php';
  $existe_id=$app->db->query("select id from nivel1 where id='".$id."'");
  valida_existencia_id($existe_id,$app);
  $borrar_valor_nivel3 = $app->db->query("delete from valor2 where nivel3 in(select nivel3.id from nivel1 inner join nivel2 on nivel2.padre=nivel1.id 
inner join nivel3 on nivel3.padre=nivel2.id where nivel1.id='".$id."' )");
  $borrar_nivel3= $app->db->query("delete from  nivel3 where id in(select nivel3.id from nivel1 inner join nivel2 on nivel2.padre=nivel1.id 
inner join nivel3 on nivel3.padre=nivel2.id where nivel1.id='".$id."' )");
  $borrar_nivel2= $app->db->query("delete from nivel2 where padre='".$id."'");
  $borrar_nivel1= $app->db->query("delete from nivel1 where id='".$id."'");
  validaDeleteNivel1($borrar_nivel1,$app,$id);
});


$app->delete('/niveles1/:id1/niveles2/:id', function($id,$id1) use($app) {

include 'librerias/validaciones.php';
  $existe_id=$app->db->query("select id from nivel2 where id='".$id."'");
  valida_existencia_id($existe_id,$app);
  $borrar_valor_nivel3 = $app->db->query("delete from valor2 where nivel3 in(select nivel3.id from nivel2 inner join nivel3 on nivel3.padre=nivel2.id 
    where nivel2.id='".$id."' and nivel2.padre='".$id1."')");
  $borrar_nivel3= $app->db->query("delete from  nivel3 where padre='".$id."'");
  $borrar_nivel2= $app->db->query("delete from nivel2 where id='".$id."' and padre='".$id1."'");
  validaDeleteNivel2($borrar_nivel2,$app,$id);
});

$app->delete('/niveles1/:id1/niveles2/:id2/niveles3/:id3', function($id1,$id2,$id3) use($app) {

include 'librerias/validaciones.php';
  $existe_id=$app->db->query("select id from nivel3 where id='".$id3."'");
  valida_existencia_id($existe_id,$app);
  $borrar_valor_nivel3 = $app->db->query("delete from valor2 where nivel3 in(select nivel3.id from nivel3 inner join nivel2 on nivel2.id=nivel3.padre inner join
    nivel1 on nivel1.id=nivel2.padre where nivel1.id='".$id1."' and nivel2.id='".$id2."' and nivel3.id='".$id3."')");
  $borrar_nivel3= $app->db->query("delete from  nivel3 where id in(select nivel3.id from nivel3 inner join nivel2 on nivel2.id=nivel3.padre inner join
    nivel1 on nivel1.id=nivel2.padre where nivel1.id='".$id1."' and nivel2.id='".$id2."' and nivel3.id='".$id3."')");
  validaDeleteNivel3($borrar_nivel3,$app,$id);
});





/*DocumentRoot - Apache
C:\hpw\.....\punlic
Solo se va a  ver la carpeta Public 
app/
	routes/
		libros.php/
	templates/
		xml/
		json/
	vendor/==>Directoriio donde coposer gestiona las dependencias del proyecto
public/
	api/
	.htacces
	index.php
*/