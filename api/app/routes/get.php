<?php
/////////////////////////////ENTIDADES/////////////////////////////////////////////////////REGRESAR XML O JSON PARA CUANDO HAYA ERRORES 
$app->get('/entidades', function () use($app) {
  $datos = array(
    'entidades' => array(
    )
  );
  $entidades = $app->db->query("select id,entidad from entidad2");
  include 'librerias/validaciones.php';
  validaResultadosQuery($entidades,$app);
  foreach ($entidades as $entidad) {
    $datos['entidades'][] = $entidad;
  }
  $app->response->setStatus(200);
  include 'librerias/atemplates.php';
  aTemplates($app,$datos);
});

$app->get('/entidades/:id', function ($id) use($app) {
  $datos = array(
    'entidad' => array(
    )
  );
    include 'librerias/validaciones.php';
    validaIds($id,$app);
    $entidad = $app->db->query("select id,entidad from entidad2 where id=$id");
    validaResultadosQuery($entidad,$app);

  foreach ($entidad as $entidad_u) {
    $datos['entidad'][] = $entidad_u;
  }

  $app->response->setStatus(200);
  include 'librerias/atemplates.php';
  aTemplates($app,$datos);
});
////////////////////////////////////////MUNICIPIOS////////////////////////////////////////////////
$app->get('/entidades/:id/municipios', function ($id) use($app) {
  $datos = array(
    'municipios' => array(
    )
  );
  include 'librerias/validaciones.php';
  validaIds($id,$app);
  $municipios = $app->db->query("select entidad2.id as id_en,municipio2.id as id_mun,municipio,codigo from 
entidad2 inner join municipio2 on municipio2.entidad=entidad2.id where entidad2.id=$id");
  validaResultadosQuery($municipios,$app);
  foreach ($municipios as $municipio) {
    $datos['municipios'][] = $municipio;
  }
  $app->response->setStatus(200);
  include 'librerias/atemplates.php';
  aTemplates($app,$datos);
});

$app->get('/entidades/:ide/municipios/:id', function ($ide,$id) use($app) {
  $datos = array(
    'municipio' => array(
    )
  );
  include 'librerias/validaciones.php';
    validaIds($id,$app);
    validaIds($ide,$app);
  $municipio = $app->db->query("select * from municipio2 where codigo='".$id."' and entidad=$ide");
 validaResultadosQuery($municipio,$app);
  foreach ($municipio as $municipio_u) {
    $datos['municipio'][] = $municipio_u;
  }
  $app->response->setStatus(200);
  include 'librerias/atemplates.php';
  aTemplates($app,$datos);
});

$app->get('/entidades/:ide/municipios/:id/indicadores', function ($ide,$id) use($app) {
  $datos = array(
    'indicadores' => array(
    )
  );
  include 'librerias/validaciones.php';
    validaIds($id,$app);
    validaIds($ide,$app);
  $indicadores = $app->db->query("select valor2.id,codigo,municipio,indicador.id_indicador as id_ind,indicador.descripcion
,anio,dato,municipio2.entidad from valor2 inner join anio on anio.valor=valor2.id inner join 
municipio2 on municipio2.id=valor2.id_mun inner join indicador on valor2.id_ind=
indicador.id_indicador where municipio2.codigo='".$id."' and municipio2.entidad=$ide order by id_indicador");
   $numero_a=$app->db->query("select count(id_ind) as num from valor2 inner join anio on anio.valor=valor2.id 
inner join municipio2 on municipio2.id=valor2.id_mun
 where codigo='".$id."' and municipio2.entidad=$ide
   group by id_ind order by id_ind  ");
   $cantidad_i=$app->db->query("select count(id_indicador) as num from indicador where id_indicador in(
    select id_ind as num from valor2 inner join anio on anio.valor=valor2.id group by id_ind
    )");
 validaResultadosQuery($indicadores,$app);
  foreach ($indicadores as $indicador_u) {
    $datos['indicadores'][] = $indicador_u;
  }
  foreach ($numero_a as $numero) {
    $datos['numeros'][] = $numero;
  }
  foreach ($cantidad_i as $cantidad) {
    $datos['cantidad_in'][] = $cantidad;
  }
  $app->response->setStatus(200);
  include 'librerias/atemplates.php';
  aTemplates($app,$datos);
});



$app->get('/entidades/:ide/municipios/:id/indicadores/:idi', function ($ide,$id,$idi) use($app) {
  $datos = array(
    'indicador' => array(
    )
  );
  include 'librerias/validaciones.php';
    validaIds($id,$app);
    validaIds($ide,$app);
  $indicador = $app->db->query("select valor2.id,codigo,municipio,indicador.id_indicador as id_ind,indicador.descripcion
,anio,dato from valor2 inner join anio on anio.valor=valor2.id inner join 
municipio2 on municipio2.id=valor2.id_mun inner join indicador on valor2.id_ind=
indicador.id_indicador where municipio2.codigo='".$id."' and municipio2.entidad=$ide and
indicador.id_indicador='".$idi."' order by anio");
     $numero_a=$app->db->query("select count(id_ind) as num from valor2 inner join anio on anio.valor=valor2.id 
      inner join municipio2 on municipio2.id=valor2.id_mun
    where id_ind='".$idi."' and municipio2.entidad=$ide and municipio2.codigo='".$id."'group by id_ind order by id_ind ");
     validaResultadosQuery($numero_a,$app);
 validaResultadosQuery($indicador,$app);
  foreach ($indicador as $indicador_u) {
    $datos['indicador'][] = $indicador_u;
  }
    foreach ($numero_a as $numero) {
    $datos['numeros'][] = $numero;
  }
  $app->response->setStatus(200);
  include 'librerias/atemplates.php';
  aTemplates($app,$datos);
});