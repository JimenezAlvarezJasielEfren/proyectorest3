<?php
function aTemplates($app,$datos){
	if($app->request->headers->get('Accept')==='application/xml'){
		$app->render('xml/get_xml.php', $datos);
		$app->response->headers->set('Content-Type', 'application/xml');
	}
  	if($app->request->headers->get('Accept')==='application/json'){
		$app->render('json/get_json.php', $datos);
		$app->response->headers->set('Content-Type', 'application/json');
	}
}
