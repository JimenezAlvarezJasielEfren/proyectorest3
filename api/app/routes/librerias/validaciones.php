<?php
function validaResultadosQuery($datos,$app){
  if(!$datos){
   if($app->request->headers->get('Accept')==='application/xml'){
    $app->response->headers->set('Content-Type', 'application/xml');
     $mensaje='<mensaje>En estos momentos no podemos acceder a la informacion, por favor intente mas tarde.</mensaje>';
     $app->halt(500, $mensaje);
   }
   if($app->request->headers->get('Accept')==='application/json'){
    $app->response->headers->set('Content-Type', 'application/json');
     $mensaje='{"mensaje":"En estos momentos no podemos acceder a la informacion, por favor intente mas tarde."}';
     $app->halt(500, $mensaje);
   }
  }

if($datos->rowCount()<1){
     if($app->request->headers->get('Accept')==='application/xml'){
      $app->response->headers->set('Content-Type', 'application/xml');
     $mensaje='<mensaje>Recurso no encontrado.</mensaje>';
     $app->halt(404, $mensaje);
   }
   if($app->request->headers->get('Accept')==='application/json'){
    $app->response->headers->set('Content-Type', 'application/json');
     $mensaje='{"mensaje":"Recurso no encontrado."}';
     $app->halt(404, $mensaje);
   }
}
}

function validaIds($id,$app){
  if(!is_numeric($id)){
    if($app->request->headers->get('Accept')==='application/xml'){
      $app->response->headers->set('Content-Type', 'application/xml');
     $mensaje='<mensaje>El recurso '.$id.' no es valido.</mensaje>';
     $app->halt(400, $mensaje);
   }
   if($app->request->headers->get('Accept')==='application/json'){
    $app->response->headers->set('Content-Type', 'application/json');
     $mensaje='{"mensaje":"El recurso '.$id.' no es valido."}';
     $app->halt(400, $mensaje);
   }
}
}
function valida_existencia_id($datos,$app){
if($datos<1){
     if($app->request->headers->get('Accept')==='application/xml'){
      $app->response->headers->set('Content-Type', 'application/xml');
     $mensaje='<mensaje>Recurso no encontrado.</mensaje>';
     $app->halt(404, $mensaje);
   }
   if($app->request->headers->get('Accept')==='application/json'){
    $app->response->headers->set('Content-Type', 'application/json');
     $mensaje='{"mensaje":"Recurso no encontrado."}';
     $app->halt(404, $mensaje);
   }
}
if($datos>=1){

}
}

function validaDelete($datos,$app,$id){
if (!$datos) {
      if($app->request->headers->get('Accept')==='application/xml'){
    $app->response->headers->set('Content-Type', 'application/xml');
     $mensaje='<mensaje>No se ha podido eliminar la entidad con id '.$id.'.</mensaje>';
     $app->halt(500, $mensaje);
  }
     if($app->request->headers->get('Accept')==='application/json'){
    $app->response->headers->set('Content-Type', 'application/json');
     $mensaje='{"mensaje":"No se ha podido eliminar la entidad con id '.$id.'."}';
     $app->halt(500, $mensaje);
   }
}
  if($app->request->headers->get('Accept')==='application/xml'){
    $app->response->headers->set('Content-Type', 'application/xml');
     $mensaje='<mensaje>La entidad con id '.$id.' ha sido eliminada con exito.</mensaje>';
     $app->halt(200, $mensaje);
  }
     if($app->request->headers->get('Accept')==='application/json'){
    $app->response->headers->set('Content-Type', 'application/json');
     $mensaje='{"mensaje":"La entidad con id '.$id.' ha sido eliminada con exito."}';
     $app->halt(200, $mensaje);
   }
}

function validaDeleteMun($datos,$app,$id){
if (!$datos) {
      if($app->request->headers->get('Accept')==='application/xml'){
    $app->response->headers->set('Content-Type', 'application/xml');
     $mensaje='<mensaje>No se ha podido eliminar el municipio con id '.$id.'.</mensaje>';
     $app->halt(500, $mensaje);
  }
     if($app->request->headers->get('Accept')==='application/json'){
    $app->response->headers->set('Content-Type', 'application/json');
     $mensaje='{"mensaje":"No se ha podido eliminar el municipio con id '.$id.'."}';
     $app->halt(500, $mensaje);
   }
}
  if($app->request->headers->get('Accept')==='application/xml'){
    $app->response->headers->set('Content-Type', 'application/xml');
     $mensaje='<mensaje>El municipio con id '.$id.' ha sido eliminada con exito.</mensaje>';
     $app->halt(200, $mensaje);
  }
     if($app->request->headers->get('Accept')==='application/json'){
    $app->response->headers->set('Content-Type', 'application/json');
     $mensaje='{"mensaje":"El municipio con id '.$id.' ha sido eliminada con exito."}';
     $app->halt(200, $mensaje);
   }
}

function validaDeleteIndicador($datos,$app,$id){
if (!$datos) {
      if($app->request->headers->get('Accept')==='application/xml'){
    $app->response->headers->set('Content-Type', 'application/xml');
     $mensaje='<mensaje>No se ha podido eliminar el indicador con id '.$id.'.</mensaje>';
     $app->halt(500, $mensaje);
  }
     if($app->request->headers->get('Accept')==='application/json'){
    $app->response->headers->set('Content-Type', 'application/json');
     $mensaje='{"mensaje":"No se ha podido eliminar el indicador con id '.$id.'."}';
     $app->halt(500, $mensaje);
   }
}
  if($app->request->headers->get('Accept')==='application/xml'){
    $app->response->headers->set('Content-Type', 'application/xml');
     $mensaje='<mensaje>El indicador con id '.$id.' ha sido eliminado con exito.</mensaje>';
     $app->halt(200, $mensaje);
  }
     if($app->request->headers->get('Accept')==='application/json'){
    $app->response->headers->set('Content-Type', 'application/json');
     $mensaje='{"mensaje":"El indicador con id '.$id.' ha sido eliminado con exito."}';
     $app->halt(200, $mensaje);
   }
}

function validaDeleteValor($datos,$app,$id){
if (!$datos) {
      if($app->request->headers->get('Accept')==='application/xml'){
    $app->response->headers->set('Content-Type', 'application/xml');
     $mensaje='<mensaje>No se ha podido eliminar el valor con id '.$id.'.</mensaje>';
     $app->halt(500, $mensaje);
  }
     if($app->request->headers->get('Accept')==='application/json'){
    $app->response->headers->set('Content-Type', 'application/json');
     $mensaje='{"mensaje":"No se ha podido eliminar el valor con id '.$id.'."}';
     $app->halt(500, $mensaje);
   }
}
  if($app->request->headers->get('Accept')==='application/xml'){
    $app->response->headers->set('Content-Type', 'application/xml');
     $mensaje='<mensaje>El valor con id '.$id.' ha sido eliminado con exito.</mensaje>';
     $app->halt(200, $mensaje);
  }
     if($app->request->headers->get('Accept')==='application/json'){
    $app->response->headers->set('Content-Type', 'application/json');
     $mensaje='{"mensaje":"El valor con id '.$id.' ha sido eliminado con exito."}';
     $app->halt(200, $mensaje);
   }
}

function validaDeleteNivel1($datos,$app,$id){
if (!$datos) {
      if($app->request->headers->get('Accept')==='application/xml'){
    $app->response->headers->set('Content-Type', 'application/xml');
     $mensaje='<mensaje>No se ha podido eliminar el nivel de categoria 1 con id '.$id.'.</mensaje>';
     $app->halt(500, $mensaje);
  }
     if($app->request->headers->get('Accept')==='application/json'){
    $app->response->headers->set('Content-Type', 'application/json');
     $mensaje='{"mensaje":"No se ha podido eliminar el nivel de categoria 1 con id '.$id.'."}';
     $app->halt(500, $mensaje);
   }
}
  if($app->request->headers->get('Accept')==='application/xml'){
    $app->response->headers->set('Content-Type', 'application/xml');
     $mensaje='<mensaje>El nivel de categoria 1 con id '.$id.' ha sido eliminado con exito.</mensaje>';
     $app->halt(200, $mensaje);
  }
     if($app->request->headers->get('Accept')==='application/json'){
    $app->response->headers->set('Content-Type', 'application/json');
     $mensaje='{"mensaje":"El nivel de categoria 1 con id '.$id.' ha sido eliminado con exito."}';
     $app->halt(200, $mensaje);
   }
}

function validaDeleteNivel2($datos,$app,$id){
if (!$datos) {
      if($app->request->headers->get('Accept')==='application/xml'){
    $app->response->headers->set('Content-Type', 'application/xml');
     $mensaje='<mensaje>No se ha podido eliminar el nivel de categoria 2 con id '.$id.'.</mensaje>';
     $app->halt(500, $mensaje);
  }
     if($app->request->headers->get('Accept')==='application/json'){
    $app->response->headers->set('Content-Type', 'application/json');
     $mensaje='{"mensaje":"No se ha podido eliminar el nivel de categoria 2 con id '.$id.'."}';
     $app->halt(500, $mensaje);
   }
}
  if($app->request->headers->get('Accept')==='application/xml'){
    $app->response->headers->set('Content-Type', 'application/xml');
     $mensaje='<mensaje>El nivel de categoria 2 con id '.$id.' ha sido eliminado con exito.</mensaje>';
     $app->halt(200, $mensaje);
  }
     if($app->request->headers->get('Accept')==='application/json'){
    $app->response->headers->set('Content-Type', 'application/json');
     $mensaje='{"mensaje":"El nivel de categoria 2 con id '.$id.' ha sido eliminado con exito."}';
     $app->halt(200, $mensaje);
   }
}

function validaDeleteNivel3($datos,$app,$id){
if (!$datos) {
      if($app->request->headers->get('Accept')==='application/xml'){
    $app->response->headers->set('Content-Type', 'application/xml');
     $mensaje='<mensaje>No se ha podido eliminar el nivel de categoria 3 con id '.$id.'.</mensaje>';
     $app->halt(500, $mensaje);
  }
     if($app->request->headers->get('Accept')==='application/json'){
    $app->response->headers->set('Content-Type', 'application/json');
     $mensaje='{"mensaje":"No se ha podido eliminar el nivel de categoria 3 con id '.$id.'."}';
     $app->halt(500, $mensaje);
   }
}
  if($app->request->headers->get('Accept')==='application/xml'){
    $app->response->headers->set('Content-Type', 'application/xml');
     $mensaje='<mensaje>El nivel de categoria 3 con id '.$id.' ha sido eliminado con exito.</mensaje>';
     $app->halt(200, $mensaje);
  }
     if($app->request->headers->get('Accept')==='application/json'){
    $app->response->headers->set('Content-Type', 'application/json');
     $mensaje='{"mensaje":"El nivel de categoria 3 con id '.$id.' ha sido eliminado con exito."}';
     $app->halt(200, $mensaje);
   }
}