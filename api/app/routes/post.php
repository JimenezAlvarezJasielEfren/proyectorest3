
<?php
$app->post('/entidades', function() use($app) {
  $nuevaEntidad=$app->request->getBody();
  $nombreEntidad="";
  $msj;
  $msjError;
try{
  if($app->request->headers->get('Content-Type')==='application/xml; charset=UTF-8'){
    $app->response->headers->set('Content-Type','application/xml');
    $entidadXML = new DomDocument('1.0','UTF-8');
    $entidadXML->loadXML($nuevaEntidad);
    $nodoEntidad=$entidadXML->getElementsByTagName("nombre");
    if($nodoEntidad->length === 1){
      $nodoNombre = $nodoEntidad->item(0);
      $nombreEntidad=$nodoNombre->nodeValue;
      $msj=
"<mensaje>
  <asunto>Se agrego una entidad</asunto>
  <entidad>$nombreEntidad</entidad>
</mensaje>";
    }
    $msjError=
"<mensaje>
  <asunto>La entidad que intentas agregar ya esta registrada (-_-)!</asunto>
  <entidad>$nombreEntidad</entidad>
</mensaje>";
  }else{
    $app->response->headers->set('Content-Type','application/json');
    $entidadJSON = json_decode($nuevaEntidad);
    $nombreEntidad = $entidadJSON->entidad->nombre;
    $msj=
'{
  "mensaje":{ 
      "asunto":"Se agrego una entidad",
      "entidad":"'.$nombreEntidad.'"
  }
}';
    $msjError=
'{
  "mensaje":{ 
      "asunto":"La entidad que intentas agregar ya esta registrada (-_-)!",
      "entidad":"'.$nombreEntidad.'"
  }
}';
  }
}catch(Exception $error){
  $app->halt(400,"Hay un problema con la sintaxis del formato que enviaste, (-_- )
Y si revisas bien la documentacion? ;)");
}
  
  $validar = $app->db->query("select * from entidad2 where entidad='$nombreEntidad'");
      if ($validar->rowCount() > 0) {
        $app->halt(404,$msjError);
      }else{  
        $entidadNueva = $app->db->query("insert into entidad2 (entidad) values('$nombreEntidad')");
        $app->halt(201,$msj);
      }
});
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*/////////////////////////////////////////////////////////////////////Agregar municipio///////////////////////////////////////////////////////*/
$app->post('/entidades/:id/municipios', function($id) use($app){
  $nuevoMunicipio=$app->request->getBody();
  $nombreMunicipio="";
  $msj;
  $msjErrorEntidad;
  $msjErrorMunicipio;
try{
  if($app->request->headers->get('Content-Type')==='application/xml; charset=UTF-8'){
    $app->response->headers->set('Content-Type','application/xml');
    $municipioXML = new DomDocument('1.0','UTF-8');
    $municipioXML->loadXML($nuevoMunicipio);
    $nodoMunicipio=$municipioXML->getElementsByTagName("nombre");
    if($nodoMunicipio->length === 1){
      $nodoNombre = $nodoMunicipio->item(0);
      $nombreMunicipio = $nodoNombre->nodeValue;
      $msj=
"<mensaje>
  <asunto>Se agrego un municipio</asunto>
  <entidad>$id</entidad>
    <municipio>$nombreMunicipio</municipio>
</mensaje>";
    }
    $msjErrorEntidad=
"<mensaje>
  <asunto>La entidad a la que intentas agregar un municipio no existe!! (-_-)!</asunto>
</mensaje>";
    $msjErrorMunicipio=
"<mensaje>
  <asunto>EL municipio que intentas agregar ya esta registrado!! (-_-)!</asunto>
  <municipio>$nombreMunicipio</municipio>
</mensaje>";
  }else{
    $app->response->headers->set('Content-Type','application/json');
    $municipioJSON = json_decode($nuevoMunicipio);
    $nombreMunicipio = $municipioJSON->municipio->nombre;
    $msj=
'{
  "mensaje":{ 
      "asunto":"Se agrego un municipio",
      "entidad":{
        "id":"'.$id.'",
        "municipio":"'.$nombreMunicipio.'"
      }
  }
}';
    $msjErrorEntidad=
'{
  "mensaje":{ 
      "asunto":"La entidad a la que intentas agregar un municipio no existe!! (-_-)!"
  }
}';
    $msjErrorMunicipio=
'{
  "mensaje":{ 
      "asunto":"EL municipio que intentas agregar ya esta registrado!! (-_-)!"
  }
}';
  }
}catch(Exception $error){
  $app->halt(400,"Hay un problema con la sintaxis del formato que enviaste, (-_- )
Y si revisas bien la documentacion?");
}
  
  $validarEntidad = $app->db->query("select *from entidad2 where id = $id");
    if($validarEntidad->rowCount() < 1)
      $app->halt(404,$msjErrorEntidad);
    else{
      $validarMunicipio = $app->db->query("select *from municipio2 where entidad=$id and municipio='$nombreMunicipio'");
      if($validarMunicipio->rowCount() === 1)
        $app->halt(404,$msjErrorMunicipio);
      else{
        $obtenerCodigo = $app->db->query("select count(entidad)+1 as codigo from municipio2 where entidad=$id");
        $codigo=1;
        foreach ($obtenerCodigo as $obt){
         $codigo=$obt['codigo'];
        }
        if($codigo < 1)
          $codigo=1;
        $nuevoMunicipio = $app->db->query("insert into municipio2 (entidad,municipio,codigo) values ($id,'$nombreMunicipio','$codigo')");
        $app->halt(201,$msj);
      }
    }
});
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////Agregar indicador///////////////////////////////////////////////////////*/
$app->post('/entidades/:idEntidad/municipios/:idMunicipio/indicadores', function($idEntidad,$idMunicipio) use($app) {
  $nuevoValor=$app->request->getBody();
  $nivel3;
  $idIndicador;
  $msj;
  $msjError;
  $msjErrorEntidad;
  $msjErrorMunicipio;
try{
  if($app->request->headers->get('Content-Type')==='application/xml; charset=UTF-8'){
    $app->response->headers->set('Content-Type','application/xml');
    $valorXML = new DomDocument('1.0','UTF-8');
    $valorXML->loadXML($nuevoValor);
    $nodoNivel = $valorXML->getElementsByTagName("nivel");
    $nodoIdIndicador=$valorXML->getElementsByTagName("idIndicador");
    if($nodoNivel->length === 1 && $nodoIdIndicador->length === 1){
      $nodoNivel3 = $nodoNivel->item(0);
      $nivel3=$nodoNivel3->nodeValue;
      $nodoIndicador=$nodoIdIndicador->item(0);
      $idIndicador=$nodoIndicador->nodeValue;
      $msj=
"<mensaje>
  <asunto>Se agrego un registro</asunto>
</mensaje>";
    }
    $msjError="
<mensaje>
  <asunto>EL indicador que intentas ingresar ya fue registrado antes a este municipio(-_-)!</asunto>
</mensaje>";
    $msjErrorEntidad=
"<mensaje>
  <asunto>La entidad con id $idEntidad no existe!! (-_-)!</asunto>
</mensaje>";
    $msjErrorMunicipio=
"<mensaje>
  <asunto>EL municipio con id: $idMunicipio no existe en esta entidad!! (-_-)!</asunto>
</mensaje>";
    $msjErrorNivel=
"<mensaje>
  <asunto>EL nivel con id: $nivel3 no existe!! (-_-)!</asunto>
</mensaje>";
    $msjErrorIndicador=
"<mensaje>
  <asunto>EL indicador con id: $idIndicador no existe!! (-_-)!</asunto>
</mensaje>";
  }
  else{
    $app->response->headers->set('Content-Type','application/json');
    $valorJSON = json_decode($nuevoValor);
    $nivel3 = $valorJSON->indicador->nivel3;
    $idIndicador = $valorJSON->indicador->idIndicador;
    $msj=
'{
  "mensaje":{ 
      "asunto":"Se agrego un registro"
  }
}';
    $msjErrorEntidad=
'{
  "mensaje":{ 
      "asunto":"La entidad con id: '.$idEntidad.' no existe!! (-_-)!"
  }
}';
    $msjErrorMunicipio=
'{
  "mensaje":{ 
      "asunto":"EL municipio con id '.$idMunicipio.' no existe en esta entidad!! (-_-)!"
  }
}';
    $msjError=
'"mensaje":{ 
      "asunto":"El indicador que intentas ingresar ya fue registado antes en este municipio(-_-)!"
  }
}';
    $msjErrorNivel=
'"mensaje":{ 
      "asunto":"El nivel con id: '.$nivel3.' no existe (-_-)!"
  }
}';
    $msjErrorIndicador=
'{
  "mensaje":{ 
      "asunto":"El indicador con id: '.$idIndicador.' no existe (-_-)!"
  }
}';
  }
}catch(Exception $error){
  $app->halt(400,"Hay un problema con la sintaxis del formato que enviaste, revisalo (¬_¬)");
}
  
    $validarEntidad = $app->db->query("select *from entidad2 where id = $idEntidad");
    if($validarEntidad->rowCount() < 1)
      $app->halt(404,$msjErrorEntidad);
    else{
      $validarMunicipio = $app->db->query("select *from municipio2 where entidad=$idEntidad and id=$idMunicipio");
      if($validarMunicipio->rowCount() < 1)
        $app->halt(404,$msjErrorMunicipio);
      else{
        $validarNivel= $app->db->query("select * from nivel3 where id='$nivel3'");
        if($validarNivel->rowCount() <1 )
          $app->halt(404,$msjErrorNivel);
        else{
          $validarIndicador = $app->db->query("select *from indicador where id_indicador='$idIndicador'");
          if($validarIndicador->rowCount() <1 )
            $app->halt(404,$msjErrorIndicador);
          else{
            $validarValor=$app->db->query("select *from valor2 where id_entidad=$idEntidad and id_mun=$idMunicipio and nivel3='$nivel3' and id_ind='$idIndicador'");
            if($validarValor->rowCount() === 1)
              $app->halt(404,$msjError);
            else{
              $registroNuevo = $app->db->query("insert into valor2 (id_entidad,id_mun,nivel3,id_ind) values ($idEntidad,$idMunicipio,'$nivel3','$idIndicador')");
              $app->halt(201,$msj);
            }
          }
        }
      }
    }
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////Agregar año  y valor a indicador///////////////////////////////////////////////////////*/
$app->post('/entidades/:idEntidad/municipios/:idMunicipio/indicadores/:idIndicador', function($idEntidad,$idMunicipio,$idIndicador) use($app) {
  $nuevoValor=$app->request->getBody();
  $idValor;
  $anio;
  $dato;
  $msj;
  $msjError;
  $msjErrorEntidad;
  $msjErrorMunicipio;
  $nivel3;
try{
  if($app->request->headers->get('Content-Type')==='application/xml; charset=UTF-8'){
    $app->response->headers->set('Content-Type','application/xml');
    $valorXML = new DomDocument('1.0','UTF-8');
    $valorXML->loadXML($nuevoValor);
    $nodoNivel = $valorXML->getElementsByTagName("anio");
    $nodoIdIndicador=$valorXML->getElementsByTagName("dato");
    if($nodoNivel->length === 1 && $nodoIdIndicador->length === 1){
      $nodoNivel3 = $nodoNivel->item(0);
      $anio=$nodoNivel3->nodeValue;
      $nodoIndicador=$nodoIdIndicador->item(0);
      $dato=$nodoIndicador->nodeValue;
      $msj=
"<mensaje>
  <asunto>Se agrego un registro</asunto>
</mensaje>";
    }
    $msjError="
<mensaje>
  <asunto>EL indicador no existe (-_-)!</asunto>
</mensaje>";
    $msjErrorEntidad=
"<mensaje>
  <asunto>La entidad con id $idEntidad no existe!! (-_-)!</asunto>
</mensaje>";
    $msjErrorMunicipio=
"<mensaje>
  <asunto>EL municipio con id: $idMunicipio no existe en esta entidad!! (-_-)!</asunto>
</mensaje>";
    $msjErrorAnio=
"<mensaje>
  <asunto>EL anio ya existe!! (-_-)!</asunto>
</mensaje>";
    $msjErrorIndicador=
"<mensaje>
  <asunto>EL indicador con id: $idIndicador no existe!! (-_-)!</asunto>
</mensaje>";
  }
  else{
    $app->response->headers->set('Content-Type','application/json');
    $valorJSON = json_decode($nuevoValor);
    $anio = $valorJSON->indicador->anio;
    $dato = $valorJSON->indicador->dato;
    $msj=
'{
  "mensaje":{ 
      "asunto":"Se agrego un registro"
  }
}';
    $msjErrorEntidad=
'{
  "mensaje":{ 
      "asunto":"La entidad con id: '.$idEntidad.' no existe!! (-_-)!"
  }
}';
    $msjErrorMunicipio=
'{
  "mensaje":{ 
      "asunto":"EL municipio con id '.$idMunicipio.' no existe en esta entidad!! (-_-)!"
  }
}';
    $msjError=
'{
  "mensaje":{ 
      "asunto":"El indicador no existe (-_-)!"
  }
}';
    $msjErrorAnio=
'"mensaje":{ 
      "asunto":"El anio ya eiste (-_-)!"
  }
}';
    $msjErrorIndicador=
'"mensaje":{ 
      "asunto":"El indicador con id: '.$idIndicador.' no existe (-_-)!"
  }
}';
  }
}catch(Exception $error){
  $app->halt(400,"Hay un problema con la sintaxis del formato que enviaste, revisalo (¬_¬)".$error);
}
  
    $validarEntidad = $app->db->query("select *from entidad2 where id = $idEntidad");
    if($validarEntidad->rowCount() < 1)
      $app->halt(404,$msjErrorEntidad);
    else{
      $validarMunicipio = $app->db->query("select *from municipio2 where entidad=$idEntidad and id=$idMunicipio");
      if($validarMunicipio->rowCount() < 1)
        $app->halt(404,$msjErrorMunicipio);
     

        else{
          $validarIndicador = $app->db->query("select *from indicador where id_indicador='$idIndicador'");
          if($validarIndicador->rowCount() <1 )
            $app->halt(404,$msjErrorIndicador);
          else{
            $validarValor=$app->db->query("select *from valor2 where id_entidad=$idEntidad and id_mun=$idMunicipio  and id_ind='$idIndicador'");
            if($validarValor->rowCount() < 1)
              $app->halt(404,$msjError);
            else{
              foreach ($validarValor as $valor)
                 $idValor=$valor['id'];
              $validarAnio=$app->db->query("select *from anio where valor=$idValor and anio='$anio'");
              if($validarAnio->rowCount() > 0)
                $app->halt(404,$msjErrorAnio);
              else{
                $registroNuevo = $app->db->query("insert into anio values ($idValor,'$anio',$dato)");
                echo "insert into anio values ($idValor,'$anio',$dato))";
                $app->halt(201,$msj);
              }
            }
          }
        }
      
    }
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////Agregar nivel 1///////////////////////////////////////////////////////
$app->post('/niveles1', function() use($app) {
  $nuevoNivel1=$app->request->getBody();
  $temaNivel1;
  $msj;
  $msjError;
try{
  if($app->request->headers->get('Content-Type')==='application/xml; charset=UTF-8'){
    $app->response->headers->set('Content-Type','application/xml');
    $nivel1XML = new DomDocument('1.0','UTF-8');
    $nivel1XML->loadXML($nuevoNivel1);
    $nodoNivel1=$nivel1XML->getElementsByTagName("tema");
    if($nodoNivel1->length === 1){
      $nodoTema = $nodoNivel1->item(0);
      $temaNivel1=$nodoTema->nodeValue;
      $msj=
"<mensaje>
  <asunto>Se agrego un nuevo tema al nivel 1<asunto>
</mensaje>";
    }
    $msjError=
"<mensaje>
  <asunto>El tema que quieres ingresar ya fue registrado anteriormente (-_-)!<asunto>
</mensaje>";
  }else{
    $app->response->headers->set('Content-Type','application/json');
    $nivel1JSON = json_decode($nuevoNivel1);
    $temaNivel1 = $nivel1JSON->nivel1->tema;
    $msj=
'{
  "mensaje":{ 
      "asunto":"Se agrego un nuevo tema al nivel 1"
  }
}';
    $msjError=
'{
  "mensaje":{ 
      "asunto":"El tema que quieres ingresar ya fue registrado anteriormente (-_-)!"
  }
}';
  }
}catch(Exception $error){
  $app->halt(400,"Hay un problema con la sintaxis del formato que enviaste, revisalo (¬_¬)");
}
  $validarNivel1 = $app->db->query("select * from nivel1 where tema='$temaNivel1'");
      if ($validarNivel1->rowCount() > 0) {
        $app->halt(404,$msjError);
      }else{
        $queryId=$app->db->query("select count(id) as id from nivel1");
        $id=0;
        foreach ($queryId as $qId) {
          $id=$qId['id'];
        }
        $registroNuevo = $app->db->query("insert into nivel1 values('$id','$temaNivel1')");
        $app->halt(201,$msj);
      }
});
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////Agregar nivel 2///////////////////////////////////////////////////////
$app->post('/niveles1/:idNivel1/niveles2', function($idNivel1) use($app) {
  $nuevoNivel2=$app->request->getBody();
  $temaNivel2;
  $msj;
  $msjError;
  $msjErrorNivel1;
try{
  if($app->request->headers->get('Content-Type')==='application/xml; charset=UTF-8'){
    $app->response->headers->set('Content-Type','application/xml');
    $nivel2XML = new DomDocument('1.0','UTF-8');
    $nivel2XML->loadXML($nuevoNivel2);
    $nodoNivel2=$nivel2XML->getElementsByTagName("tema");
    if($nodoNivel2->length === 1){
      $nodoTema = $nodoNivel2->item(0);
      $temaNivel2=$nodoTema->nodeValue;
      $msj=
"<mensaje>
  <asunto>Se agrego un nuevo tema al nivel 2</asunto>
</mensaje>";
    }
    $msjError=
"<mensaje>
  <asunto>El tema que quieres ingresar ya fue registrado anteriormente (-_-)!</asunto>
</mensaje>";
    $msjErrorNivel1=
"<mensaje>
  <asunto>El nivel 1 con id: $idNivel1 no existe (-_-)!</asunto>
</mensaje>";
  }else{
    $app->response->headers->set('Content-Type','application/json');
    $nivel2JSON = json_decode($nuevoNivel2);
    $temaNivel2 = $nivel2JSON->nivel2->tema;
    $msj=
'{
  "mensaje":{ 
      "asunto":"Se agrego un nuevo tema al nivel 2"
  }
}';
    $msjError=
'{
  "mensaje":{ 
      "asunto":"El tema que quieres ingresar ya fue registrado anteriormente (-_-)!"
  }
}';
    $msjErrorNivel1=
'{
  "mensaje":{ 
      "asunto":"El nivel 1 con id: '.$idNivel1.' no existe (-_-)!"
  }
}';
  }
}catch(Exception $error){
  $app->halt(400,"Hay un problema con la sintaxis del formato que enviaste, revisalo (¬_¬)");
}
  
  $validarNivel1 = $app->db->query("select * from nivel1 where id='$idNivel1'");
      if ($validarNivel1->rowCount() < 1)
        $app->halt(404,$msjErrorNivel1);
      else{
        $validarNivel2=$app->db->query("select *from nivel2 where tema='$temaNivel2'");
        if($validarNivel2->rowCount()>0)
          $app->halt(404,$msjError);
        else{
          $queryId=$app->db->query("select count(id) as id from nivel2");
          $id=0;
          foreach($queryId as $qId) {
            $id=$qId['id'];
          }
          $registroNuevo = $app->db->query("insert into nivel2 values('$id','$idNivel1','$temaNivel2')");
          $app->halt(201,$msj);
        }
      }
});
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////Agregar nivel 3///////////////////////////////////////////////////////
$app->post('/niveles1/:idNivel1/niveles2/:idNivel2/niveles3', function($idNivel1,$idNivel2) use($app) {
  $nuevoNivel3=$app->request->getBody();
  $temaNivel3;
  $msj;
  $msjError;
  $msjErrorNivel1;
  $msjErrorNivel2;
try{
  if($app->request->headers->get('Content-Type')==='application/xml; charset=UTF-8'){
    $app->response->headers->set('Content-Type','application/xml');
    $nivel3XML = new DomDocument('1.0','UTF-8');
    $nivel3XML->loadXML($nuevoNivel3);
    $nodoNivel3=$nivel3XML->getElementsByTagName("tema");
    if($nodoNivel3->length === 1){
      $nodoTema = $nodoNivel3->item(0);
      $temaNivel3=$nodoTema->nodeValue;
      $msj=
"<mensaje>
  <asunto>Se agrego un nuevo tema al nivel 3</asunto>
</mensaje>";
    }
    $msjError=
"<mensaje>
  <asunto>El tema que quieres ingresar ya fue registrado anteriormente (-_-)!</asunto>
</mensaje>";
    $msjErrorNivel1=
"<mensaje>
  <asunto>El nivel 1 con id: $idNivel1 no existe (-_-)!</asunto>
</mensaje>";
    $msjErrorNivel2=
"<mensaje>
  <asunto>El nivel 2 con id: $idNivel2 no existe (-_-)!</asunto>
</mensaje>";
  }else{
    $app->response->headers->set('Content-Type','application/json');
    $nivel3JSON = json_decode($nuevoNivel3);
    $temaNivel3 = $nivel3JSON->nivel3->tema;
    $msj=
'{
  "mensaje":{ 
      "asunto":"Se agrego un nuevo tema al nivel 3"
  }
}';
    $msjError=
'{
  "mensaje":{ 
      "asunto":"El tema que quieres ingresar ya fue registrado anteriormente (-_-)!"
  }
}';
    $msjErrorNivel1=
'{
  "mensaje":{ 
      "asunto":"El nivel 1 con id: '.$idNivel1.' no existe (-_-)!"
  }
}';
    $msjErrorNivel2=
'{
  "mensaje":{ 
      "asunto":"El nivel 2 con id: '.$idNivel2.' no existe (-_-)!"
  }
}';

  }
}catch(Exception $error){
  $app->halt(400,"Hay un problema con la sintaxis del formato que enviaste, revisalo (¬_¬)".$error);
}
  
      $validarNivel1 = $app->db->query("select * from nivel1 where id='$idNivel1'");
      if ($validarNivel1->rowCount() < 1)
        $app->halt(404,$msjErrorNivel1);
      else{
        $validarNivel2=$app->db->query("select *from nivel2 where id='$idNivel2'");
        if($validarNivel2->rowCount() < 1)
          $app->halt(404,$msjErrorNivel2);
        else{
          $validarNivel3=$app->db->query("select *from nivel3 where tema='$temaNivel3'");
          if($validarNivel3->rowCount() > 0)
            $app->halt(404,$msjError);
          else{
           $queryId=$app->db->query("select count(id) as id from nivel3");
            $id=0;
            foreach($queryId as $qId) {
              $id=$qId['id'];
            }
            $registroNuevo = $app->db->query("insert into nivel3 values('$id','$idNivel2','$temaNivel3')");
            $app->halt(201,$msj);
        }
        }
      }
});
?>