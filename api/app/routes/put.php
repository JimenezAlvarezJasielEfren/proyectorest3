
<?php
$app->put('/entidades/:idEntidad', function($idEntidad) use($app) {
  $nuevaEntidad=$app->request->getBody();
  $nombreEntidad;
  $errorSintaxis;
  $msj;
  $msjError;
  $msjErrorNombre;
try{
  if($app->request->headers->get('Content-Type')==='application/xml; charset=UTF-8'){
"<mensaje>
<asunto>Hay un problema con la sintaxis en el XML</asunto>
</mensaje>";
      $app->response->headers->set('Content-Type','application/xml');
      $entidadXML = new DomDocument('1.0','UTF-8');
      $entidadXML->loadXML($nuevaEntidad);
      $nodoEntidad=$entidadXML->getElementsByTagName("nuevoNombre");
      if($nodoEntidad->length === 1){
        $nodoNombre = $nodoEntidad->item(0);
        $nombreEntidad=$nodoNombre->nodeValue;
        $msj=
"<mensaje>
  <asunto>Se actualizo una entidad</asunto>
  <entidad>$nombreEntidad</entidad>
</mensaje>";
      }
      $msjError=
"<mensaje>
  <asunto>La entidad que intentas actualizar no existe (-_-)!</asunto>
  <entidad>$nombreEntidad</entidad>
</mensaje>";
      $msjErrorNombre=
"<mensaje>
  <asunto>El nombre nuevo ya pertence a otra entidad, intenta con otro ;)!</asunto>
  <entidad>$nombreEntidad</entidad>
</mensaje>";
  }else{
    $errorSintaxis=
'{
  "mensaje":{ 
      "asunto":"Hay un problema con la sintaxis en el JSON"      
  }
}';
    $app->response->headers->set('Content-Type','application/json');
    $entidadJSON = json_decode($nuevaEntidad);
    $nombreEntidad = $entidadJSON->entidad->nombre;
    $msj=
'{
  "mensaje":{ 
      "asunto":"Se actualizo una entidad",
      "entidad":"'.$nombreEntidad.'"
  }
}';
    $msjError=
'{
  "mensaje":{ 
      "asunto":"La entidad que intentas actualizar no existe (-_-)!",
      "entidad":"'.$nombreEntidad.'"
  }
}';
  $msjErrorNombre=
'{
  "mensaje":{ 
      "asunto":"El nombre nuevo ya pertences a otra entidad, intenta con otro ;)!",
      "entidad":"'.$nombreEntidad.'"
  }
}';
  }
}catch(Exception $error){
  $app->halt(404,$errorSintaxis);
}
  
  $validar = $app->db->query("select * from entidad2 where id=$idEntidad");
      if ($validar->rowCount() < 1) {
        $app->halt(404,$msjError);
      }else{  
        $validarNombre = $app->db->query("select *from entidad2 where entidad='$nombreEntidad'");
        if($validarNombre->rowCount() > 0)
          $app->halt(404,$msjErrorNombre);
        else{
          $entidadNueva = $app->db->query("update entidad2 set entidad='$nombreEntidad' where id=$idEntidad");
          $app->halt(201,$msj);
      }
      }
});
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*/////////////////////////////////////////////////////////////////////Agregar municipio///////////////////////////////////////////////////////*/
$app->put('/entidades/:id/municipios/:idMunicipio', function($id,$idMunicipio) use($app){
  $nuevoMunicipio=$app->request->getBody();
  $nombreMunicipio="";
  $errorSintaxis;
  $msj;
  $msjErrorEntidad;
  $msjErrorMunicipio;
  $msjErrorNombreMunicipio;
try{
  if($app->request->headers->get('Content-Type')==='application/xml; charset=UTF-8'){
    $errorSintaxis=
"<mensaje>
<asunto>Hay un problema con la sintaxis en el XML</asunto>
</mensaje>";
    $app->response->headers->set('Content-Type','application/xml');
    $municipioXML = new DomDocument('1.0','UTF-8');
    $municipioXML->loadXML($nuevoMunicipio);
    $nodoMunicipio=$municipioXML->getElementsByTagName("nombre");
    if($nodoMunicipio->length === 1){
      $nodoNombre = $nodoMunicipio->item(0);
      $nombreMunicipio = $nodoNombre->nodeValue;
      $msj=
"<mensaje>
  <asunto>Se actualizo un municipio</asunto>
  <entidad>$id</entidad>
    <municipio>$nombreMunicipio</municipio>
</mensaje>";
    }
    $msjErrorEntidad=
"<mensaje>
  <asunto>La entidad no existe!! (-_-)!</asunto>
</mensaje>";
    $msjErrorMunicipio=
"<mensaje>
  <asunto>EL municipio que intentas actualizar no existe!! (-_-)!</asunto>
  <municipio>$nombreMunicipio</municipio>
</mensaje>";
    $msjErrorNombreMunicipio=
"<mensaje>
  <asunto>Ya existe un muncipio con ese nombre en esta entidad!!, intenta con otro nombre ;)</asunto>
  <municipio>$nombreMunicipio</municipio>
</mensaje>";
  }else{
    $errorSintaxis=
'{
  "mensaje":{ 
      "asunto":"Hay un problema con la sintaxis en el JSON"      
  }
}';
    $app->response->headers->set('Content-Type','application/json');
    $municipioJSON = json_decode($nuevoMunicipio);
    $nombreMunicipio = $municipioJSON->municipio->nombre;
    $msj=
'{
  "mensaje":{ 
      "asunto":"Se actualizo un municipio",
      "entidad":"{
        "id":"'.$id.'",
        "municipio":"'.$nombreMunicipio.'"
      }
  }
}';
    $msjErrorEntidad=
'{
  "mensaje":{ 
      "asunto":"La entidad a la que intentas actualizar no existe!! (-_-)!"
  }
}';
    $msjErrorMunicipio=
'{
  "mensaje":{ 
      "asunto":"EL municipio que intentas actualizar no existe!! (-_-)!"
  }
}';
    $msjErrorNombreMunicipio=
'{
  "mensaje":{ 
      "asunto":"Ya existe un municipio con ese nombre en esa entidad!!, intenta con otro nombre ;)"
  }
}';
  }
}catch(Exception $error){
  $app->halt(400,$errorSintaxis);
}
  
  $validarEntidad = $app->db->query("select *from entidad2 where id = $id");
    if($validarEntidad->rowCount() < 1)
      $app->halt(404,$msjErrorEntidad);
    else{
      $validarMunicipio = $app->db->query("select *from municipio2 where entidad=$id and codigo='$idMunicipio'");
      if($validarMunicipio->rowCount() < 1)
        $app->halt(404,$msjErrorMunicipio);
      else{
        $validarNombreMunicipio=$app->db->query("select * from municipio2 where entidad = $id and codigo='idMunicipio'");
        if($validarNombreMunicipio->rowCount() > 0)
          $app->halt(404,$msjErrorNombreMunicipio);
        else{
          $nuevoMunicipio = $app->db->query("update municipio2 set municipio='$nombreMunicipio' where entidad=$id and codigo='$idMunicipio'");
          $app->halt(201,$msj);
        }
      }
    }
});
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*/////////////////////////////////////////////////////////////////////Agregar indicador///////////////////////////////////////////////////////*/
$app->put('/indicadores/:idIndicador',function ($idIndicador) use($app){
  $nuevoIndicador=$app->request->getBody();
  $idIndicador;
  $descripcionIndicador;
  $notaIndicador;
  $msj;
  $errorSintaxis;
  $msjError;
try{
  $errorSintaxis=
"<mensaje>
<asunto>Hay un problema con la sintaxis en el XML</asunto>
</mensaje>";
  if($app->request->headers->get('Content-Type')==='application/xml; charset=UTF-8'){
    $app->response->headers->set('Content-Type','application/xml');
    $indicadorXML = new DomDocument('1.0','UTF-8');
    $indicadorXML->loadXML($nuevoIndicador);
    $nodoTema = $indicadorXML->getElementsByTagName("descripcion");
    $nodoAnotaciones = $indicadorXML->getElementsByTagName("nota");

    if($nodoTema->length===1 && $nodoAnotaciones->length===1){
      $nodoDescripcion=$nodoTema->item(0);
      $descripcionIndicador=$nodoDescripcion->nodeValue;
      $nodoNotas=$nodoAnotaciones->item(0);
      $notaIndicador=$nodoNotas->nodeValue;
      $msj=
"<mensaje>
  <asunto>Se actualizo un indicador</asunto>
  <id>$idIndicador</id>
  <descripcion>$descripcionIndicador</descripcion>
  <nota>$notaIndicador</nota>
</mensaje>";
    }
    $msjError=
"<mensaje>
  <asunto>El indicador que intentas actualizar no existe (-_-)!</asunto>
  <indicador>$descripcionIndicador</indicador>
</mensaje>";
    $msjErrorDescripcion=
"<mensaje>
  <asunto>La descripcion ya existe para otro indicador</asunto>
</mensaje>";
  }else{
    $errorSintaxis=
'{
  "mensaje":{ 
      "asunto":"Hay un problema con la sintaxis en el JSON"      
  }
}';
    $app->response->headers->set('Content-Type','application/json');
    $indicadorJSON = json_decode($nuevoIndicador);
    $descripcionIndicador=$indicadorJSON->indicador->descripcion;
    $notaIndicador=$indicadorJSON->indicador->nota;
    $msj=
'{
  "mensaje":{ 
      "asunto":"Se actualizo un indicador",
      "id":"'.$idIndicador.'",
      "descripcion":"'.$descripcionIndicador.'",
      "nota":"'.$notaIndicador.'"
  }
}';
    $msjError=
'{
  "mensaje":{ 
      "asunto":"El indicador que intentas actualizar no existe (-_-)!",
      "indicador":"'.$descripcionIndicador.'"
  }
}';
    $msjErrorDescripcion=
'{
  "mensaje":{ 
      "asunto":"La descripcion ya existe para otro indicador"
  }
}';
  }
}catch(Exception $error){
  $app->halt(400,$errorSintaxis);
}
  
  $validar = $app->db->query("select * from indicador where id_indicador='$idIndicador'");
      if ($validar->rowCount() < 1) {
        $app->halt(404,$msjError);
      }else{
        $validarDescripcion = $app->db->query("select *from indicador where descripcion = '$descripcionIndicador'");
        if($validarDescripcion->rowCount() > 0)
          $app->halt(404,$msjErrorDescripcion);
        else{
          $registroNuevo = $app->db->query("update indicador set descripcion = '$descripcionIndicador', nota = '$notaIndicador' where id_indicador = '$idIndicador'");
          $app->halt(201,$msj);
        }
      }
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////Agregar valor///////////////////////////////////////////////////////*/
$app->put('/valores/:idValor', function($idValor) use($app) {
  $nuevoValor=$app->request->getBody();
  $nivel3;
  $idIndicador;
  $errorSintaxis;
  $msj;
  $msjError;
  $msjErrorEntidad;
  $msjErrorMunicipio;
  $idEntidad;
  $idMunicipio;
  $mjsErrorDuplicado;
try{
  if($app->request->headers->get('Content-Type')==='application/xml; charset=UTF-8'){
  $errorSintaxis=
"<mensaje>
<asunto>Hay un problema con la sintaxis en el XML</asunto>
</mensaje>";
    $app->response->headers->set('Content-Type','application/xml');
    $valorXML = new DomDocument('1.0','UTF-8');
    $valorXML->loadXML($nuevoValor);
    $nodoEntidad = $valorXML->getElementsByTagName("idEntidad");
    $nodoMunicipio = $valorXML->getElementsByTagName("idMunicipio");
    $nodoNivel = $valorXML->getElementsByTagName("idNivel3");
    $nodoIdIndicador=$valorXML->getElementsByTagName("idIndicador");
    if($nodoNivel->length === 1 && $nodoIdIndicador->length === 1 && $nodoEntidad->length ===1 && $nodoMunicipio->length ===1 ){
      $nodoEnt = $nodoEntidad->item(0);
      $idEntidad = $nodoEnt->nodeValue;
      $nodoMpio = $nodoMunicipio->item(0);
      $idMunicipio = $nodoMpio->nodeValue;
      $nodoNivel3 = $nodoNivel->item(0);
      $nivel3=$nodoNivel3->nodeValue;
      $nodoIndicador=$nodoIdIndicador->item(0);
      $idIndicador=$nodoIndicador->nodeValue;
      $msj=
"<mensaje>
  <asunto>Se actualizo un registro valores</asunto>
</mensaje>";
    }
    $msjError="
<mensaje>
  <asunto>EL valor que intentas actualizar no existe (-_-)!</asunto>
</mensaje>";
    $msjErrorEntidad=
"<mensaje>
  <asunto>La entidad con id $idEntidad no existe!! (-_-)!</asunto>
</mensaje>";
    $msjErrorMunicipio=
"<mensaje>
  <asunto>EL municipio con id: $idMunicipio no existe en esta entidad!! (-_-)!</asunto>
</mensaje>";
    $msjErrorNivel=
"<mensaje>
  <asunto>EL nivel con id: $nivel3 no existe!! (-_-)!</asunto>
</mensaje>";
    $msjErrorIndicador=
"<mensaje>
  <asunto>EL indicador con id: $idIndicador no existe!! (-_-)!</asunto>
</mensaje>";
    $mjsErrorDuplicado=
"<mensaje>
  <asunto>Ya existe un valor con los datos que estas agregando!! (-_-)!</asunto>
</mensaje>";
    $msjErrorValor=
"<mensaje>
  <asunto>No existe un registro valor con id $idValor !! (-_-)!</asunto>
</mensaje>";
  }
  else{
    $errorSintaxis=
'{
  "mensaje":{ 
      "asunto":"Hay un problema con la sintaxis en el JSON"      
  }
}';

    $app->response->headers->set('Content-Type','application/json');
    $valorJSON = json_decode($nuevoValor);
    $idEntidad = $valorJSON->valor->idEntidad;
    $idMunicipio = $valorJSON->valor->idMunicipio;
    $nivel3 = $valorJSON->valor->idNivel3;
    $idIndicador = $valorJSON->valor->idIndicador;
    $msj=
'{
  "mensaje":{ 
      "asunto":"Se actualizo un registro a valores"
  }
}';
    $msjErrorEntidad=
'{
  "mensaje":{ 
      "asunto":"La entidad con id: '.$idEntidad.' no existe!! (-_-)!"
  }
}';
    $msjErrorMunicipio=
'{
  "mensaje":{ 
      "asunto":"EL municipio con id '.$idMunicipio.' no existe en esta entidad!! (-_-)!"
  }
}';
    $msjError=
'"mensaje":{ 
      "asunto":"El valor que intentas actualizar no existe (-_-)!"
  }
}';
    $msjErrorNivel=
'"mensaje":{ 
      "asunto":"El nivel con id: '.$nivel3.' no existe (-_-)!"
  }
}';
    $msjErrorIndicador=
'"mensaje":{ 
      "asunto":"El indicador con id: '.$idIndicador.' no existe (-_-)!"
  }
}';
    $mjsErrorDuplicado=
'"mensaje":{ 
      "asunto":"ya existe un valor con los datos que estas agregando (-_-)!"
  }
}';
    $msjErrorValor=
'"mensaje":{ 
      "asunto":"No existe un registro valor con id: '.$idValor.'(-_-)!"
  }
}';
  }
}catch(Exception $error){
  $app->halt(400,$errorSintaxis);
}
  
    $validarEntidad = $app->db->query("select *from entidad2 where id = $idEntidad");
    if($validarEntidad->rowCount() < 1)
      $app->halt(404,$msjErrorEntidad);
    else{
      $validarMunicipio = $app->db->query("select *from municipio2 where entidad=$idEntidad and codigo='$idMunicipio'");
      if($validarMunicipio->rowCount() < 1)
        $app->halt(404,$msjErrorMunicipio);
      else{
        $validarNivel= $app->db->query("select * from nivel3 where id='$nivel3'");
        if($validarNivel->rowCount() <1 )
          $app->halt(404,$msjErrorNivel);
        else{
          $validarIndicador = $app->db->query("select *from indicador where id_indicador='$idIndicador'");
          if($validarIndicador->rowCount() <1 )
            $app->halt(404,$msjErrorIndicador);
          else{
            $validarValor=$app->db->query("select *from valor2 where id_entidad=$idEntidad and id_mun=$idMunicipio and nivel3='$nivel3' and id_ind='$idIndicador'");
            if($validarValor->rowCount() > 0)
              $app->halt(404,$mjsErrorDuplicado);
            else{
              $validarIdValor=$app->db->query("select *from valor2 where id = $idValor");
              if($validarIdValor->rowCount() < 1)
                $app->halt(404,$msjErrorValor);
              else{
                $registroNuevo = $app->db->query("update valor2 set id_entidad=$idEntidad, id_mun=$idMunicipio , nivel3='$nivel3', id_ind = '$idIndicador' where id = $idValor");
                $app->halt(201,$msj);
              }
            }
          }
        }
      }
    }
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////Agregar valor a un año///////////////////////////////////////////////////////
$app->put('/valores/:idValor/anios/:anio', function($idValor,$anio) use($app) {
  $nuevoAnio=$app->request->getBody();
  $anio;
  $datoAnio;
  $errorSintaxis;
  $msj;
  $msjError;
  $msjErrorValor;
try{
  if($app->request->headers->get('Content-Type')==='application/xml; charset=UTF-8'){
    $errorSintaxis=
"<mensaje>
<asunto>Hay un problema con la sintaxis en el XML</asunto>
</mensaje>";
    $app->response->headers->set('Content-Type','application/xml');
    $anioXML = new DomDocument('1.0','UTF-8');
    $anioXML->loadXML($nuevoAnio);
    $nodoDatoAnio=$anioXML->getElementsByTagName("dato");
    if($nodoDatoAnio->length===1){
      $nodoDato=$nodoDatoAnio->item(0);
      $datoAnio=$nodoDato->nodeValue;
      $msj=
"<mensaje>
  <asunto>Se actualizo el valor del año $anio</asunto>
</mensaje>";
    }
    $msjError=
"<mensaje>
  <asunto>El año no  existe(-_-)!</asunto>
</mensaje>";
    $msjErrorValor=
"<mensaje>
  <asunto>No hay ningun registro en valor con id: $idValor (-_-)!</asunto>
</mensaje>";
  }else{
    $errorSintaxis=
'{
  "mensaje":{ 
      "asunto":"Hay un problema con la sintaxis en el JSON"      
  }
}';

    $app->response->headers->set('Content-Type','application/json');
    $anioJSON = json_decode($nuevoAnio);
    $datoAnio = $anioJSON->nuevoAnio->dato;
    $msj=
'{
  "mensaje":{ 
      "asunto":"Se actulaizo el valor del año '.$anio.'"
  }
}';
    $msjError=
'{
  "mensaje":{ 
      "asunto":"El anio que intentas actualizar no existe (-_-)!"
  }
}';
    $msjErrorValor=
'{
  "mensaje":{ 
      "asunto":"No hay ningun registro en valor  con id: '.$idValor.' (-_-)!"
  }
}';
  }
}catch(Exception $error){
  $app->halt(400,$errorSintaxis);
}
  
  $validarValor = $app->db->query("select * from valor2 where id=$idValor");
      if ($validarValor->rowCount() <  1)
        $app->halt(404,$msjErrorValor);
        else{
        $validarAnio = $app->db->query("select *from anio where valor=$idValor and anio='$anio'");
          if($validarAnio->rowCount() < 1)
            $app->halt(404,$msjError);
          else{
            $registroNuevo=$app->db->query("update anio set dato=$datoAnio where anio='$anio' and valor = '$idValor'");
            $app->halt(201,$msj);
        }
      }
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////Agregar nivel 1///////////////////////////////////////////////////////
$app->put('/niveles1/:idNivel1', function($idNivel1) use($app) {
  $nuevoNivel1=$app->request->getBody();
  $temaNivel1;
  $errorSintaxis;
  $msj;
  $mjsErrorDuplicado;
  $msjError;
try{
  if($app->request->headers->get('Content-Type')==='application/xml; charset=UTF-8'){
$errorSintaxis=
"<mensaje>
<asunto>Hay un problema con la sintaxis en el XML</asunto>
</mensaje>";
    $app->response->headers->set('Content-Type','application/xml');
    $nivel1XML = new DomDocument('1.0','UTF-8');
    $nivel1XML->loadXML($nuevoNivel1);
    $nodoNivel1=$nivel1XML->getElementsByTagName("tema");
    if($nodoNivel1->length === 1){
      $nodoTema = $nodoNivel1->item(0);
      $temaNivel1=$nodoTema->nodeValue;
      $msj=
"<mensaje>
  <asunto>Se actualizo un tema en el nivel 1</asunto>
</mensaje>";
    }
    $msjError=
"<mensaje>
  <asunto>El tema que quieres actualizar no existe (-_-)!<asunto>
</mensaje>";
    $mjsErrorDuplicado=
"<mensaje>
  <asunto>El tema $temaNivel1 ya existe(-_-)!<asunto>
</mensaje>";
  }else{
    $errorSintaxis=
'{
  "mensaje":{ 
      "asunto":"Hay un problema con la sintaxis en el JSON"      
  }
}';
    $app->response->headers->set('Content-Type','application/json');
    $nivel1JSON = json_decode($nuevoNivel1);
    $temaNivel1 = $nivel1JSON->nivel1->tema;
    $msj=
'{
  "mensaje":{ 
      "asunto":"Se agrego un nuevo tema al nivel 1"
  }
}';
    $msjError=
'{
  "mensaje":{ 
      "asunto":"El tema que quieres actualizar no existe (-_-)!"
  }
}';
    $mjsErrorDuplicado=
'{
  "mensaje":{ 
      "asunto":"El tema '.$temaNivel1.' ya existe (-_-)!"
  }
}';
  }
}catch(Exception $error){
  $app->halt(400,$errorSintaxis);
}
  $validarNivel1 = $app->db->query("select * from nivel1 where id ='$idNivel1'");
      if ($validarNivel1->rowCount() < 1) {
        $app->halt(404,$msjError);
      }else{
        $validarTema=$app->db->query("select *from nivel1 where tema = '$temaNivel1'");
        if($validarTema->rowCount() > 0)
          $app->halt(404,$mjsErrorDuplicado);
        else{
         $registroNuevo = $app->db->query("update nivel1 set tema = '$temaNivel1' where id='$idNivel1'");
          $app->halt(201,$msj);
        }
      }
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////Agregar nivel 2///////////////////////////////////////////////////////
$app->put('/niveles1/:idNivel1/niveles2/:idNivel2', function($idNivel1,$idNivel2) use($app) {
  $nuevoNivel2=$app->request->getBody();
  $temaNivel2;
  $errorSintaxis;
  $msj;
  $msjError;
  $msjErrorNivel1;
try{
  if($app->request->headers->get('Content-Type')==='application/xml; charset=UTF-8'){
    $errorSintaxis=
"<mensaje>
<asunto>Hay un problema con la sintaxis en el XML</asunto>
</mensaje>";
    $app->response->headers->set('Content-Type','application/xml');
    $nivel2XML = new DomDocument('1.0','UTF-8');
    $nivel2XML->loadXML($nuevoNivel2);
    $nodoNivel2=$nivel2XML->getElementsByTagName("tema");
    if($nodoNivel2->length === 1){
      $nodoTema = $nodoNivel2->item(0);
      $temaNivel2=$nodoTema->nodeValue;
      $msj=
"<mensaje>
  <asunto>Se actualizo un registro al nivel 2</asunto>
</mensaje>";
    }
    $msjError=
"<mensaje>
  <asunto>El tema que quieres actualizar no existe (-_-)!</asunto>
</mensaje>";
    $msjErrorNivel1=
"<mensaje>
  <asunto>El nivel 1 con id: $idNivel1 no existe (-_-)!</asunto>
</mensaje>";
    $mjsErrorDuplicado=
"<mensaje>
  <asunto>El tema $temaNivel2 ya existe(-_-)!<asunto>
</mensaje>";
  }else{
    $errorSintaxis=
'{
  "mensaje":{ 
      "asunto":"Hay un problema con la sintaxis en el JSON"      
  }
}';

    $app->response->headers->set('Content-Type','application/json');
    $nivel2JSON = json_decode($nuevoNivel2);
    $temaNivel2 = $nivel2JSON->nivel2->tema;
    $msj=
'{
  "mensaje":{ 
      "asunto":"Se actualizo un registro en el nivel 2"
  }
}';
    $msjError=
'{
  "mensaje":{ 
      "asunto":"El tema que quieres actualizar no existe (-_-)!"
  }
}';
    $msjErrorNivel1=
'{
  "mensaje":{ 
      "asunto":"El nivel 1 con id: '.$idNivel1.' no existe (-_-)!"
  }
}';
    $mjsErrorDuplicado=
'{
  "mensaje":{ 
      "asunto":"El tema '.$temaNivel2.' ya existe (-_-)!"
  }
}';
  }
}catch(Exception $error){
  $app->halt(400,$errorSintaxis);
}
  
  $validarNivel1 = $app->db->query("select * from nivel1 where id='$idNivel1'");
      if ($validarNivel1->rowCount() < 1)
        $app->halt(404,$msjErrorNivel1);
      else{
        $validarNivel2=$app->db->query("select *from nivel2 where id='$idNivel2'");
        if($validarNivel2->rowCount() < 1)
          $app->halt(404,$msjError);
        else{
          $validarTemaNivel2=$app->db->query("select *from nivel2 where tema='$temaNivel2'");
          if($validarTemaNivel2->rowCount() > 0)
            $app->halt(404,$mjsErrorDuplicado);
          else{
           $registroNuevo = $app->db->query("update nivel2 set tema='$temaNivel2' where id='$idNivel2'");
           $app->halt(201,$msj);
          }
        }
      }
});
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////Agregar nivel 3///////////////////////////////////////////////////////
$app->put('/niveles1/:idNivel1/niveles2/:idNivel2/niveles3/:idNivel3', function($idNivel1,$idNivel2,$idNivel3) use($app) {
  $nuevoNivel3=$app->request->getBody();
  $temaNivel3;
  $msj;
  $msjError;
  $msjErrorNivel1;
  $msjErrorNivel2;
try{
  if($app->request->headers->get('Content-Type')==='application/xml; charset=UTF-8'){
    $errorSintaxis=
"<mensaje>
<asunto>Hay un problema con la sintaxis en el XML</asunto>
</mensaje>";
    $app->response->headers->set('Content-Type','application/xml');
    $nivel3XML = new DomDocument('1.0','UTF-8');
    $nivel3XML->loadXML($nuevoNivel3);
    $nodoNivel3=$nivel3XML->getElementsByTagName("tema");
    if($nodoNivel3->length === 1){
      $nodoTema = $nodoNivel3->item(0);
      $temaNivel3=$nodoTema->nodeValue;
      $msj=
"<mensaje>
  <asunto>Se actualizo un registro al nivel 3</asunto>
</mensaje>";
    }
    $msjError=
"<mensaje>
  <asunto>El tema que quieres actualizar no existe (-_-)!</asunto>
</mensaje>";
    $msjErrorNivel1=
"<mensaje>
  <asunto>El nivel 1 con id: $idNivel1 no existe (-_-)!</asunto>
</mensaje>";
    $msjErrorNivel2=
"<mensaje>
  <asunto>El nivel 2 con id: $idNivel2 no existe (-_-)!</asunto>
</mensaje>";
    $mjsErrorDuplicado=
"<mensaje>
  <asunto>El tema $temaNivel3 ya existe(-_-)!<asunto>
</mensaje>";
  }else{
    $errorSintaxis=
'{
  "mensaje":{ 
      "asunto":"Hay un problema con la sintaxis en el JSON"      
  }
}';
    $app->response->headers->set('Content-Type','application/json');
    $nivel3JSON = json_decode($nuevoNivel3);
    $temaNivel3 = $nivel3JSON->nivel3->tema;
    $msj=
'{
  "mensaje":{ 
      "asunto":"Se actualizo un registro al nivel 3"
  }
}';
    $msjError=
'{
  "mensaje":{ 
      "asunto":"El tema que quieres actualizar no existe (-_-)!"
  }
}';
    $msjErrorNivel1=
'{
  "mensaje":{ 
      "asunto":"El nivel 1 con id: '.$idNivel1.' no existe (-_-)!"
  }
}';
    $msjErrorNivel2=
'{
  "mensaje":{ 
      "asunto":"El nivel 2 con id: '.$idNivel2.' no existe (-_-)!"
  }
}';
    $mjsErrorDuplicado=
'{
  "mensaje":{ 
      "asunto":"El tema '.$temaNivel3.' ya existe (-_-)!"
  }
}';

  }
}catch(Exception $error){
  $app->halt(400,$errorSintaxis);
}
  
      $validarNivel1 = $app->db->query("select * from nivel1 where id='$idNivel1'");
      if ($validarNivel1->rowCount() < 1)
        $app->halt(404,$msjErrorNivel1);
      else{
        $validarNivel2=$app->db->query("select *from nivel2 where id='$idNivel2'");
        if($validarNivel2->rowCount() < 1)
          $app->halt(404,$msjErrorNivel2);
        else{
          $validarNivel3=$app->db->query("select *from nivel3 where id='$idNivel3'");
          if($validarNivel3->rowCount() < 1)
            $app->halt(404,$msjError);
          else{
           $validarTemaNivel3 = ("select *from nivel3 where tema='$temaNivel3'");
           if($validarTemaNivel3 > 0)
            $msj->halt(404,$mjsErrorDuplicado);
           else{
            $registroNuevo = $app->db->query("update nivel3 set tema='$temaNivel3' where id='$idNivel3'");
            $app->halt(201,$msj);
          }
        }
        }
      }
});
?>