<?php 
if (isset($entidades)){ ?>
<entidades>
<?php 
foreach ($entidades as $b) { ?>
  <entidad>
      <id><?php echo $b['id'];?></id>  
      <nombre><?php echo $b['entidad'];?></nombre>   
  </entidad>
<?php }?>
</entidades>
<?php }?>
<?php 
if (isset($entidad)){ ?>
<?php 
foreach ($entidad as $b) { ?>
  <entidad>
      <id><?php echo $b['id'];?></id>  
      <nombre><?php echo $b['entidad'];?></nombre>   
  </entidad>
  <?php }?>
<?php }?>
<?php
if (isset($municipios)){ ?>
<entidad id="<?php echo $municipios[0]['id_en']; ?>">
<?php 
foreach ($municipios as $b) { ?>
  <municipio>
      <id><?php echo $b['id_mun'];?></id>  
      <nombre><?php echo $b['municipio'];?></nombre>
      <codigo><?php echo $b['codigo'];?></codigo>   
  </municipio>
  <?php }?>
</entidad>
<?php }?>
<?php
if (isset($municipio)){ ?>
<?php 
foreach ($municipio as $b) { ?>
  <municipio>
      <id><?php echo $b['id'];?></id>  
      <entidad><?php echo $b['entidad'];?></entidad>
      <nombre><?php echo $b['municipio'];?></nombre>
      <codigo><?php echo $b['codigo'];?></codigo>   
  </municipio>
  <?php }?>
<?php }?>
<?php
if (isset($indicadores)){ 
$n=0;
?>
<indicadores>
<?php for ($i=0;$i< count($indicadores);$i+=$numeros[$n-1]['num']) { ?>
  <indicador>
      <id><?php echo $indicadores[$i]['id_ind'];?></id>  
      <descripcion><?php echo $indicadores[$i]['descripcion'];?></descripcion>
      <periodos>
        <?php for ($j=0;$j<$numeros[$n]['num'];$j++) { ?>
            <periodo>
              <anio><?php echo $indicadores[$i+$j]['anio'];?></anio>  
              <dato><?php echo $indicadores[$i+$j]['dato'];?></dato>         
            </periodo>
        <?php }?>
      </periodos>  
  </indicador>
  <?php $n++;}?>
</indicadores>
<?php }?>
<?php
if (isset($indicador)){ 
$n=0;
$i=0;
?>
  <indicador>
      <id><?php echo $indicador[$i]['id_ind'];?></id>  
      <descripcion><?php echo $indicador[$i]['descripcion'];?></descripcion>
      <periodos>
        <?php for ($j=0;$j<$numeros[$n]['num'];$j++) { ?>
            <periodo>
              <anio><?php echo $indicador[$i+$j]['anio'];?></anio>  
              <dato><?php echo $indicador[$i+$j]['dato'];?></dato>         
            </periodo>
        <?php }?>
      </periodos>  
  </indicador>
<?php }?>
