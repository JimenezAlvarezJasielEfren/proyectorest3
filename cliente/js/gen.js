    
	function asociar_hijo(obj_padre,obj_hijo){
		obj_padre.appendChild(obj_hijo);
	}
    
	function ejecutar(){
        s=document.getElementsByName("anio");
        indice=s[0].selectedIndex;
        valor=parseInt(s[0][indice].value);
        dibuja(valor);
    }
			
			
	obj = document.createElement("button");
    obj.addEventListener('click',ejecutar,false);
    obj.innerHTML="Graficar";
    obj.setAttribute("id","Graficar");
	obj.setAttribute("class","btn btn-primary");
	
    asociar_hijo(document.body,obj);
		function dibuja(anioValor){
		var texto;
		if(anioValor===0)
				texto='Total de accidentes por entidad';
			else
				texto='Total de accidentes en el año '+anioValor;
				
			  $(function () {
					
                var codigo=['mx-ag','mx-bc','mx-bs','mx-cm','mx-co','mx-cl','mx-cs','mx-ch','mx-df','mx-dg','mx-gj','mx-gr','mx-hg','mx-ja','mx-mx',
							'mx-mi','mx-mo','mx-na','mx-nl','mx-oa','mx-pu','mx-qr','mx-qt','mx-sl','mx-si','mx-so','mx-tb','mx-tm','mx-tl','mx-ve',
							'mx-yu','mx-za'];

                var data=new Array();
                var xmls=new Array();
                data[0]={ 'hc-key': 'mx-3622', value: 0 };
                edo=1;
                for(var i=2; i<=33;i++){
                    var dato=0;
                    mpio=i-1;
                    var datos="http://localhost/api/public/api/entidades/"+i+"/municipios/"+mpio+"/indicadores";
                    
                    var xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function() {
                        
                       
                      if (this.readyState === 4) {
                            if (this.status === 200) {
                                
                                var documento_xml = xhr.responseXML;
                                
                                valores=documento_xml.getElementsByTagName('periodo');

                                
                                
                                for (var j = 0; j < valores.length; j++) {
                                    evento=valores[j];
									anio=parseInt(valores[j].children[0].innerHTML)
									if(anioValor===0)
										dato+=parseInt(valores[j].children[1].innerHTML);
									else
										if(anio === anioValor)
											dato=parseInt(valores[j].children[1].innerHTML);
                                }
                                data[edo]={'hc-key':codigo[edo-1], value:dato};                                                                                                            
                        }
                    }
                }
                    xhr.open('GET',datos,false);
                    xhr.setRequestHeader("Accept","application/xml");
                    xhr.send();
                    edo++;
                
                }
                // Initiate the chart
                $('#container').highcharts('Map', {
                    
                    title : {
                        text : texto
                    },

                    subtitle : {
                        text : ''
                    },

                    mapNavigation: {
                        enabled: true,
                        buttonOptions: {
                            verticalAlign: 'bottom'
                        }
                    },

                    colorAxis: {
                        min: 0
                    },

                    series : [{
                        data : data,
                        mapData: Highcharts.maps['countries/mx/mx-all'],
                        joinBy: 'hc-key',
                        name: texto,
                        states: {
                            hover: {
                                color: '#BADA55'
                            }
                        },
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }]
                });
            });
			}
			dibuja(0);