entidades_arr=new Array();
nombres_arr=new Array();
nombres_arr_mun=new Array();
codigo_mun=new Array();
codigo_ent=new Array();
ids_arr=new Array();
entidad_mun=new Array();
grafica_entidad="";
grafica_municipio="";
function asociar_hijo(obj,padre)
{
    padre.appendChild(obj);
}

function crea_form(componentes){
    var form_temporal=document.createElement("form");
    form_temporal.setAttribute("role","form");
        var div_temp=document.createElement("div");
        div_temp.setAttribute("class","form-group");
        asociar_hijo(componentes["etiqueta"],div_temp);
        asociar_hijo(componentes["input"],div_temp);
        asociar_hijo(div_temp,form_temporal);   
    return form_temporal;
}

function crea_select(funcion,name,etiqueta,id,opciones,ids_o){
    var select_temp=document.createElement("select");
    select_temp.setAttribute("id","select_e");
    for(var i=0;i<opciones.length;i++){
        var opcion=document.createElement("option");
        opcion.setAttribute("value",opciones[i]);
        opcion.setAttribute("id",ids_o[i]);
        opcion.textContent=opciones[i];
        asociar_hijo(opcion,select_temp);
    }
    var etiqueta_temporal=document.createElement("label");
    etiqueta_temporal.textContent=etiqueta;
    etiqueta_temporal.setAttribute("class","lead");
    var obj={"etiqueta":etiqueta_temporal,"input":select_temp};
    return obj;
}

function crea_select_mun(name,etiqueta,id,opciones,ids_o,id_ent){
    var select_temp=document.createElement("select");
    select_temp.setAttribute("id",ids_o[i]);
    for(var i=0;i<opciones.length;i++){
        var opcion=document.createElement("option");
        opcion.setAttribute("value",opciones[i]);
        opcion.setAttribute("codigo",ids_o[i]);
        opcion.setAttribute("entidad",id_ent[i]); 
        opcion.textContent=opciones[i];
        asociar_hijo(opcion,select_temp);
    }
    var etiqueta_temporal=document.createElement("label");
    etiqueta_temporal.textContent=etiqueta;
    etiqueta_temporal.setAttribute("class","lead");
    var obj={"etiqueta":etiqueta_temporal,"input":select_temp};
    return obj;
}

function  obtener_entidades(){
    var url_en="http://localhost/api/public/api/entidades";
    var xhr_en = new XMLHttpRequest();
    xhr_en.onreadystatechange = function() {
      if (this.readyState === 4) {
        if (this.status === 200) {
          var documento_xml_en = xhr_en.responseXML;
             var values=new Array();
             var values_id=new Array();
             values= documento_xml_en.getElementsByTagName('nombre')
             values_id= documento_xml_en.getElementsByTagName('id')
    for(var i=0;i<values.length;i++){
            nombres_arr[i]=values[i].innerHTML;
            codigo_ent[i]=values_id[i].innerHTML;
    }
        } else if (this.status === 403) {
            archivo_a_descargar.value = "Error: 403 Forbidden.";
        } else if (this.status === 404) {
            archivo_a_descargar.value = "Error: 404 Not Found.";
        } else {
            archivo_a_descargar.value = "Error: " + xhr_en.status;
        }
      }
    };
    xhr_en.open('GET', url_en,false);
    xhr_en.setRequestHeader("Accept", "application/xml");
    xhr_en.send();
}

function obtenerMunicipios(id_e){
var url_mun="http://localhost/api/public/api/entidades/"+parseFloat(id_e)+"/municipios";
    var xhr_mun = new XMLHttpRequest();
    xhr_mun.onreadystatechange = function() {
      if (this.readyState === 4) {
        if (this.status === 200) {
          var documento_xml_mun = xhr_mun.responseXML;
             var values=new Array();
             var values_cod=new Array();
             var values_entidad=new Array();
             values= documento_xml_mun.getElementsByTagName('nombre')
             values_cod= documento_xml_mun.getElementsByTagName('codigo')
             values_entidad= documento_xml_mun.getElementsByTagName('entidad')
    for(var i=0;i<values.length;i++){
            nombres_arr_mun[i]=values[i].innerHTML;
            codigo_mun[i]=values_cod[i].innerHTML;
            entidad_mun[i]=values_entidad[i].innerHTML;
    }
        } else if (this.status === 403) {
            archivo_a_descargar.value = "Error: 403 Forbidden.";
        } else if (this.status === 404) {
            archivo_a_descargar.value = "Error: 404 Not Found.";
        } else {
            archivo_a_descargar.value = "Error: " + xhr_mun.status;
        }
      }
    };
    xhr_mun.open('GET', url_mun,false);
    xhr_mun.setRequestHeader("Accept", "application/xml");
    xhr_mun.send();
}
function crear_input_button(name,etiqueta,id){
    var obj;
    var input_temporal=document.createElement("a");
    input_temporal.setAttribute("name",name);
    input_temporal.setAttribute("href","#");
    input_temporal.setAttribute("id",id);
    input_temporal.setAttribute("class","btn btn-success");
    input_temporal.textContent=etiqueta;
    var etiqueta_temporal=document.createElement("label");
    etiqueta_temporal.textContent=etiqueta;
    etiqueta_temporal.setAttribute("class","lead");
    obj={"etiqueta":etiqueta_temporal,"input":input_temporal};
    return obj;
}
function funcion_select(){
 var myselect = document.getElementById("select_e");
 if(myselect){
 obtenerMunicipios(myselect.options[myselect.selectedIndex].id);
  var myselect_m = document.getElementById("select_m");
 actualizar();
}
}
function actualizar(){
select_municipio=crea_select_mun("municipio","Municipio: ","idsex",nombres_arr_mun,codigo_mun,entidad_mun);
 foo=crea_form(select_municipio);
var temp=document.getElementById("select_m");
temp=foo;
}
function graficar(){

 var myselect = document.getElementById("select_e");
 var myselect_m = document.getElementById("select_m");
 if(myselect){
grafica_entidad=myselect.options[myselect.selectedIndex].id;
//grafica_municipio=myselect_m.options[myselect_m.selectedIndex].value;
if(grafica_entidad!="1"){
creaGrafica(grafica_entidad,(parseInt(grafica_entidad)-1));
}
else{
   creaGrafica(grafica_entidad,"000"); 
}
}
}
obtener_entidades();
obtenerMunicipios(1);
boton=crear_input_button("boton","Graficar","id_boton");
select_estado=crea_select(funcion_select(),"entidad","Entidad: ","idsex",nombres_arr,codigo_ent);
var fo=crea_form(select_estado);
document.body.appendChild(fo);
select_municipio=crea_select_mun("municipio","Municipio: ","idsex",nombres_arr_mun,codigo_mun,entidad_mun);
 foo=crea_form(select_municipio);
  fooo=crea_form(boton);
//document.body.appendChild(foo);
document.body.appendChild(fooo);
evento1=document.getElementById("select_e");
evento1.addEventListener("click",funcion_select,"false");
b=document.getElementById("id_boton");
b.addEventListener("click",graficar,"false");
